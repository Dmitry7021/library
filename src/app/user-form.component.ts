import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from './services/user.service';


const emailRegex = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.less']
})
export class UserFormComponent {

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(emailRegex),
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  });

  constructor(
    private userService: UserService,
    private dialogRef: MatDialogRef<UserFormComponent>
  ) { }

  registration(): void {
    this.userService
      .create(this.form.value)
      .then(id => {
        this.dialogRef.close(id);
      })
      .catch(error => alert(error.message));
  }

  logIn(): void {
    this.userService
      .logIn(this.form.value)
      .then(() => {
        this.dialogRef.close();
      })
      .catch(error => alert(error.message));
  }

}
