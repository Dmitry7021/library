import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BooksService, IBookDoc } from './services/books.service';


const bookDefValues: IBookDoc = {
  name: '',
  year: null,
  isbn: '',
  authors: []
};

function isbnValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const isbn = control.value.replace(/(\s|-)/g, '');
    if (isbn === '') {
      return null;
    }

    const result = isbn.length === 10 ? validateIsbn10(isbn) : validateIsbn13(isbn);
    return result ? null : {isbn: {value: control.value}};
  };
}

function validateIsbn10(isbn: string): boolean {
  if (!/^\d{9}(\d|X)$/.test(isbn)) {
    return false;
  }

  let sum = 0;
  // Высчитываем сумму произведений каждой цифры isbn на её коэффициент
  // isbn          2 2 6 6 1 1 1 5 6 6
  // коэффициент  10 9 8 7 6 5 4 3 2 1
  for (let i = 0; i < 9; i++) {
    sum += +isbn[i] * (10 - i);
  }
  // Для последней цифры isbn спец обработка, т.к. там может быть римская 10 (X)
  sum += isbn[9] === 'X' ? 10 : +isbn[9];

  // В итоге получившаяся сумма должна быть кратна 11
  return sum % 11 === 0;
}

function validateIsbn13(isbn: string): boolean {
  if (!/^\d{13}$/.test(isbn)) {
    return false;
  }

  let sum = 0;
  // Числа стоящие на четных позициях умножаем на 3 и складываем
  // Числа стоящие на не четных складываем как есть
  // Последнюю цифру не трогаем, т.к. это контрольная с ней будем сравнивать
  // результат вычислений
  for (let i = 0; i < 12; i++) {
    sum += +isbn[i] * ((i + 1) % 2 === 0 ? 3 : 1);
  }

  // Высчитываем контрольную цифру: из 10 вычитаем цифру, стоящую в 1 разряде полученной суммы.
  // Если полученное число равно 10, то заменяем его на 0
  let checkDigit = 10 - sum % 10;
  if (checkDigit === 10) {
    checkDigit = 0;
  }

  // Полученная контрольная цифра должна равняться последней цифре в isbn
  return checkDigit === +isbn[12];
}

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.less']
})
export class BookFormComponent implements OnInit {

  bookId: string;

  model = new FormGroup({
    name: new FormControl('', [
      Validators.required
    ]),
    authors: new FormControl([]),
    year: new FormControl('', [
      Validators.max(new Date().getFullYear())
    ]),
    isbn: new FormControl('', [
      isbnValidator()
    ])
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private booksService: BooksService
  ) {}

  ngOnInit(): void {
    const book = this.route.snapshot.data.book;

    // Если есть данные книги, то присваиваем в модель
    if (book) {
      this.bookId = book.id;

      delete book.id;
      this.model.setValue({...bookDefValues, ...book});
    }
  }

  save(): void {
    const bookData = {
      id: this.bookId,
      ...this.model.value
    };

    this.booksService
      .saveBook(bookData)
      .then(() => this.router.navigate(['']));
  }

}
