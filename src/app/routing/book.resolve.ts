import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BooksService } from '../services/books.service';

/**
 * По параметру :id в роуте подтягивает данные книги
 */
@Injectable({
  providedIn: 'root'
})
export class BookResolve implements Resolve<any> {

  constructor(
    private booksService: BooksService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.booksService.getBookById(route.paramMap.get('id'));
  }

}
