import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from './services/user.service';
import { UserFormComponent } from './user-form.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  animations: [
    trigger('flyOut', [
      transition(':leave', [
        animate(200, style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class AppComponent {

  constructor(
    public userService: UserService,
    private matDialog: MatDialog,
  ) {}

  showAuthDialog(): void {
    this.matDialog.open(UserFormComponent, {
      width: '300px',
    });
  }
}
