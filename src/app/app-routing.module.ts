import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookFormComponent } from './book-form.component';
import { BooksListComponent } from './books-list.component';
import { AuthGuard } from './routing/auth.guard';
import { BookResolve } from './routing/book.resolve';

const routes: Routes = [
  {
    path: 'books/edit/:id',
    component: BookFormComponent,
    // Редактировать книги могут только аутентифицированные пользователи
    canActivate: [AuthGuard],
    resolve: {
      // Что бы не было морганий при переходе на форму редактирования
      // сразу подтянем данные книги
      book: BookResolve
    }
  },
  {
    path: 'books/edit',
    component: BookFormComponent,
    // Создавать книги могут только аутентифицированные пользователи
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: BooksListComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
