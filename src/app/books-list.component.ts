import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { BooksService, IBook } from './services/books.service';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.less'],
  animations: [
    trigger('flyInOut', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate(300, style({ transform: 'translateX(0)' }))
      ]),
      transition(':leave', [
        animate(300, style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class BooksListComponent {

  constructor(
    public userService: UserService,
    public booksService: BooksService,
  ) { }

  // Возвращает уникальное значение для каждой книги
  // Нужно для того, что бы при получении нового списка перерисовывались только
  // измененные книги
  getBookHash(index: number, book: IBook): string {
    return Object.keys(book).reduce((res, key) => res + book[key], '');
  }
}
