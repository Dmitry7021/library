import { Directive, ElementRef, forwardRef, HostListener, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: 'input[type=text][appArrayValue], textarea[appArrayValue]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ArrayValueDirective),
      multi: true
    }
  ]
})
export class ArrayValueDirective implements OnInit, ControlValueAccessor {

  /**
   * Строка, который должны разделяться значения поля.
   * Если не задана, то по умолчанию используется запятая.
   */
  // tslint:disable-next-line
  @Input('appArrayValue')
  separator: string;

  private elem: HTMLInputElement | HTMLTextAreaElement;

  private value: string[] = [];

  constructor(
    elem: ElementRef<HTMLInputElement | HTMLTextAreaElement>
  ) {
    this.elem = elem.nativeElement;
  }

  ngOnInit(): void {
    if (!this.separator) {
      this.separator = ',';
    }
  }

  @HostListener('blur')
  onBlur(): void {
    this.value = this.parseValue(this.elem.value);
    this.onChange(this.value);
  }

  private parseValue(value: string): string[] {
    return value.split(this.separator).map(v => v.trim()).filter(v => !!v);
  }

  private renderValue(): void {
    this.elem.value = this.value.join(`${this.separator} `);
  }

  //region ControlValueAccessor
  private onChange = (_) => {};
  private onTouched = () => {};

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    if (Array.isArray(value)) {
      this.value = value;
    }
    else if (typeof value === 'string') {
      this.value = this.parseValue(value);
    }
    else {
      throw new Error(`Unexpected value type: ${typeof value}`);
    }

    this.renderValue();
  }
  //endregion

}
