import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { FirebaseService } from './firebase.service';


export interface IUser {
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService extends FirebaseService {

  isAuthenticatedUser = false;

  private get users(): firebase.firestore.CollectionReference {
    return this.db.collection('users');
  }

  constructor() {
    super();
  }

  /**
   * Создает нового пользователя.
   * В случае успеха возвращает его id.
   * Может закончится ошибкой если пользователь с указанным email уже существует
   */
  create(user: IUser): Promise<string> {
    return this
      .isUniqueEmail(user.email)
      .then(uniq => {
        if (!uniq) {
          throw new Error(`Пользователь с email '${user.email}' уже существует`);
        }

        return this.users.add(user);
      })
      .then(docRef => {
        this.isAuthenticatedUser = true;
        return docRef.id;
      });
  }

  logIn(user: IUser): Promise<void> {
    return this
      .isUserExists(user)
      .then(exists => {
        if (!exists) {
          throw new Error('Пользователь с таким логином и паролем не существует');
        }

        this.isAuthenticatedUser = true;
      });
  }

  private isUniqueEmail(email: string): Promise<boolean> {
    return this.users
      .where('email', '==', email)
      .get()
      .then(snapshot => snapshot.empty);
  }

  private isUserExists(user: IUser): Promise<boolean> {
    return this.users
      .where('email', '==', user.email)
      .where('password', '==', user.password)
      .get()
      .then(snapshot => !snapshot.empty);
  }
}
