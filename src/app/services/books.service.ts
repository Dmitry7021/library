import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs';
import { FirebaseService } from './firebase.service';


const bookConverter = {
  toFirestore(book: IBook): firebase.firestore.DocumentData {
    const docData = {...book};
    delete docData.id;

    return docData;
  },
  fromFirestore(
    snapshot: firebase.firestore.QueryDocumentSnapshot,
    options: firebase.firestore.SnapshotOptions
  ): IBook {
    return {
      id: snapshot.id,
      ...snapshot.data(options)
    } as IBook;
  }
};

// Интерфейс представляет структуру данных документа
// в котором хранится информация о книге
export interface IBookDoc {
  name: string;
  year: number;
  isbn: string;
  authors: string[];
}

export interface IBook extends IBookDoc {
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class BooksService extends FirebaseService {

  // Коллекция, через которую работаем с книгами на сервере
  private booksCollection: firebase.firestore.CollectionReference;

  private _books$ = new BehaviorSubject<IBook[]>([]);
  /**
   * Поток в котором содержится актуальный список книг.
   * При любом изменении книги, будет выпущено новое значение списка
   */
  public books$ = this._books$.asObservable();

  constructor() {
    super();

    this.booksCollection = this.db.collection('books').withConverter(bookConverter);
    // Подписываемся на обновление списка книг на сервере что бы иметь актуальные данные
    this.booksCollection.onSnapshot({
      next: (snapshot) => {
        this._books$.next(
          snapshot.docs.map(doc => doc.data() as IBook)
        );
      }
    });
  }

  /**
   * Возвращает данные конкретной книги по её id из БД.
   * Если книги с указанной id нет, то вернется undefined.
   */
  getBookById(id: string): Promise<IBook | undefined> {
    return this.booksCollection
      .doc(id)
      .get()
      .then(doc => doc.exists ? doc.data() as IBook : undefined);
  }

  /**
   * В зависимости от наличия в переданных данных поля id выполняет
   * либо создание книги, если id не указана, либо обновление её данных.
   */
  saveBook(book: IBook): Promise<void> {
    return book.id ? this.updateBook(book) : this.createBook(book);
  }

  createBook(book: IBook): Promise<void> {
    return this.booksCollection.add(book).then(() => {});
  }

  updateBook(book: IBook): Promise<void> {
    const docData = bookConverter.toFirestore(book);
    return this.booksCollection.doc(book.id).update(docData);
  }

  deleteBook(id: string): Promise<void> {
    return this.booksCollection.doc(id).delete();
  }
}
