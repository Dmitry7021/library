import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyA7McxnIYMzOphc3KMxYC6fYvQi7WVQ26M',
  authDomain: 'library-cc659.firebaseapp.com',
  databaseURL: 'https://library-cc659.firebaseio.com',
  projectId: 'library-cc659',
  storageBucket: 'library-cc659.appspot.com',
  messagingSenderId: '1085904699264',
  appId: '1:1085904699264:web:119257d5157a74b4160b65',
  measurementId: 'G-FXPV5MTVEH'
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  protected db: firebase.firestore.Firestore = db;

  constructor() { }
}
